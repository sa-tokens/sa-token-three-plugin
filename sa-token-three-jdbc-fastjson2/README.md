## sa-token-three-jdbc-fastjson2

### 插件介绍
Sa-Token 数据库持久化插件。

### 使用方式
1. 引入插件：
``` xml
<dependency>
    <groupId>cn.dev33</groupId>
    <artifactId>sa-token-three-jdbc-fastjson2</artifactId>
    <version>${sa-token.version}</version>
</dependency>
```

2. 在 application.yml 配置数据源：
``` yaml
spring:
  datasource:
    url: jdbc:mysql://localhost:3306/test
    username: test
    password: test
```

3. 在启动类配置注解：
``` java
@MapperScan(basePackages = "cn.dev33.satoken.dao.mapper")
```

4. 在数据库新增表结构，以下为 MySQL 的表结构：
``` mysql
# 本插件使用 mybatis-plus 作为 ORM 框架， mybatis-plus 支持的数据库本插件应该都支持
# token_value 的长度可根据实际需求进行修改

CREATE TABLE `sa_token_data` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `token_key` varchar(255) NOT NULL COMMENT '键',
  `token_value` varchar(1000) NOT NULL COMMENT '值',
  `expire_time` bigint NOT NULL COMMENT '过期时间戳',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_key` (`token_key`)
) ENGINE=InnoDB;
```

### 联系方式
使用时如遇问题，请在 sa-token-three-plugin 中提交 issue 咨询