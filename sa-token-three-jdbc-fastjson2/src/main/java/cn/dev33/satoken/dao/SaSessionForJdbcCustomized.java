/*
 * Copyright 2020-2099 sa-token.cc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.dev33.satoken.dao;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.util.SaFoxUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.annotation.JSONField;


/**
 * 定制版 SaSession
 *
 * @author moon69
 * @since 1.37.0
 */
public class SaSessionForJdbcCustomized extends SaSession {

    private static final long serialVersionUID = 4855910561516650589L;

    public SaSessionForJdbcCustomized() {
        super();
    }

    public SaSessionForJdbcCustomized(String id) {
        super(id);
    }

    @Override
    public <T> T getModel(String key, Class<T> cs) {
        if (SaFoxUtil.isBasicType(cs)) {
            return SaFoxUtil.getValueByType(get(key), cs);
        }

        return JSON.parseObject(getString(key), cs);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getModel(String key, Class<T> cs, Object defaultValue) {
        Object value = get(key);
        if (valueIsNull(value)) {
            return (T) defaultValue;
        }

        if (SaFoxUtil.isBasicType(cs)) {
            return SaFoxUtil.getValueByType(get(key), cs);
        }

        return JSON.parseObject(getString(key), cs);
    }

    /**
     * 忽略 timeout 字段的序列化
     */
    @Override
    @JSONField(serialize = false)
    public long getTimeout() {
        return super.getTimeout();
    }
}
